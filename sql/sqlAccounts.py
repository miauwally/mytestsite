import sql.sqlCmd as sqlCmd

def add(email, name, pwd):
    query = ("INSERT INTO chihlee.accounts "
            "(email, name, pwd) "
            "VALUES (%s, %s, %s)")

    data = (email, name, pwd)
    lastRowID = sqlCmd.insert(query, data)

    return lastRowID

def selectByEmail(email):
    query = ("SELECT * FROM chihlee.accounts WHERE email=%s;")
    data = (email, )

    rows, fieldNames = sqlCmd.selectEx(query, data)

    if len(rows) < 1:
        return None

    return sqlCmd.dictFormatCustom(rows[0], fieldNames)

def selectAll():
    query = ("SELECT * FROM chihlee.accounts;")
    data = None

    rows, fieldNames = sqlCmd.selectEx(query, data)

    if len(rows) < 1:
        return None

    return sqlCmd.dictFormatListCustom(rows, fieldNames)
