from django.urls import include, path

import api.service as service

urlpatterns = [
    path('login', service.login),
    path('register', service.register),
]
