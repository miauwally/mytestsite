from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.files.storage import FileSystemStorage

import json
import os
from passlib.hash import argon2

import misc.httpHelper as httpHelper
import sql.sqlAccounts as sqlAccounts

@csrf_exempt
def register(request):
    email = httpHelper.getValue(request, 'email')
    name = httpHelper.getValue(request, 'name')
    password = httpHelper.getValue(request, 'password')

    account = sqlAccounts.selectByEmail(email)

    if account:
        return httpHelper.rspError('EMAIL_EXISTED')

    hashed = argon2.using(rounds=4).hash(password)
    sqlAccounts.add(email, name, hashed)

    rsp = {
        'message': 'OK',
    }

    return httpHelper.rspJson(rsp)

@csrf_exempt
def login(request):
    email = httpHelper.getValue(request, 'email')
    password = httpHelper.getValue(request, 'password')

    account = sqlAccounts.selectByEmail(email)

    if not account:
        return httpHelper.rspError('INVALID_EMAIL')

    print(password, account['pwd'])
    
    if not argon2.verify(password, account['pwd']):
        return httpHelper.rspError('INVALID_PASSWORD')

    rsp = {
        'message': 'OK',
    }

    return httpHelper.rspJson(rsp)
