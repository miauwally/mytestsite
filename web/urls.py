from django.urls import include, path
from django.conf.urls import url, include

import web.views as views

urlpatterns = [
    path('accountList', views.accountList),
    path('login', views.login),
    path('register', views.register),
    url('.*', views.register),
]
