from django.shortcuts import render

import sql.sqlAccounts as sqlAccounts

# Create your views here.
def register(request):
    page = render(request, 'register.html', {
    })

    return page

def accountList(request):
    accounts = sqlAccounts.selectAll()

    for acct in accounts:
        print(acct['name'])
    page = render(request, 'accountList.html', {
        'accounts': accounts,
    })

    return page

def login(request):
    page = render(request, 'login.html', {
    })

    return page
