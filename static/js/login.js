function Login() {
    $('#spHint').text('登入中...')
    // 檢查 email
    var email = $('#email').val()

    if (!isValidEmail(email)) {
        $('#spHint').text('Email 格式不符...')
        return
    }

    var password = $('#password').val() 

    var postData = {
        email: email,
        password: password,
    }

    AjaxPost('/api/login', postData, cbLogin, cbLoginErr)
}

function cbLogin(ret) {
    $('#spHint').text(ret.message)
}

function cbLoginErr(ret) {
    err = ret.responseJSON
    $('#spHint').text(err.message)
}
